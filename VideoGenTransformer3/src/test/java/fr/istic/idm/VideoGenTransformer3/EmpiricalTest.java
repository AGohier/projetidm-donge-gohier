package fr.istic.idm.VideoGenTransformer3;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;

import org.eclipse.emf.common.util.URI;
import org.junit.Test;

import fr.istic.idm.VideoGenTransformer3.model.VideoGenHelper;
import fr.istic.videoGen.VideoGeneratorModel;
import fr.istic.idm.VideoGenTransformer3.model.VideoUtilities;

public class EmpiricalTest {
	
	
	@Test
	public void testGeneratePlaylist() throws IOException {
		
		int orangina=0;
		int oranginaR=0;
		int charalG=0;
		int charalMord=0;
		int charalChien=0;
		int charalDV=0;
		int liptonCD=0;
		int jdO=0;
		int pandaKK=0;
		int total=0;
		
		for(int i = 0;i<10000;i++) {
			
			VideoGeneratorModel videoGen = new VideoGenHelper().loadVideoGenerator(URI.createURI("videoModel/MaVideo.videogen"));
			VideoUtilities videoUtilities = new VideoUtilities();
			videoUtilities.generatePlaylist(videoGen);
			File playList = new File("poolVideo/play.txt");
			
			BufferedReader bufferedReader = new BufferedReader(new FileReader(playList));
			String line;
			while((line=bufferedReader.readLine()) != null) {
				switch (line) {
					case "Orangina.MTS":
						orangina++;
						break;
					case "OranginaRouge.MTS":
						oranginaR++;
						break;
					case "charalGepar.MTS":
						charalG++;
						break;
					case "CharalMordus.MTS":
						charalMord++;
						break;
					case "DisparitionVegetariens.MTS":
						charalDV++;
						break;
					case "CharalLeChien.MTS":
						charalChien++;
						break;
					case "jeuDeLOie.MTS":
						jdO++;
						break;
					case "CaDetend.MTS":
						liptonCD++;
						break;
					case "PandaKitKat.MTS":
						pandaKK++;
						break;
				}
			}
			total++;
			System.out.println(total);
			bufferedReader.close();
			
		}
		
		File file = new File("videogenFiles/empiricalTest.csv");
		
		PrintWriter pw= null;
		try {
			 pw = new PrintWriter(file);
		} catch (FileNotFoundException e) {e.printStackTrace();}
		StringBuilder sb = new StringBuilder();
		
		sb.append("Total,Orangina,OranginaRouge,CharalGepar,CharalMordus,DisparitionVegeratiens,CharalLeChien,JeuDeLOie,CaDetend,PandaKitKat, \n");
		sb.append(total+","+orangina+","+oranginaR+","+charalG+","+charalMord+","+charalDV+","+charalChien+","+jdO+","+liptonCD+","+pandaKK+", \n");
		sb.append(100+","+orangina*100/total+","+oranginaR*100/total+","+charalG*100/total+","+charalMord*100/total+","+charalDV*100/total+","+charalChien*100/total+","+jdO*100/total+","+liptonCD*100/total+","+pandaKK*100/total+", \n");
		
		
		// écriture dans le fichier
		pw.write(sb.toString());
        pw.close();
		
	}
	
	@Test
	public void testVariante1() throws IOException {
		
		int orangina=0;
		int oranginaR=0;
		int charalG=0;
		int charalMord=0;
		int charalChien=0;
		int charalDV=0;
		int liptonCD=0;
		int jdO=0;
		int pandaKK=0;
		int total=0;
		
		for(int i = 0;i<100;i++) {
			
			VideoGeneratorModel videoGen = new VideoGenHelper().loadVideoGenerator(URI.createURI("videoModel/variante3.videogen"));
			VideoUtilities videoUtilities = new VideoUtilities();
			videoUtilities.generatePlaylist(videoGen);
			File playList = new File("poolVideo/play.txt");
			
			BufferedReader bufferedReader = new BufferedReader(new FileReader(playList));
			String line;
			while((line=bufferedReader.readLine()) != null) {
				switch (line) {
					case "Orangina.MTS":
						orangina++;
						break;
					case "OranginaRouge.MTS":
						oranginaR++;
						break;
					case "charalGepar.MTS":
						charalG++;
						break;
					case "CharalMordus.MTS":
						charalMord++;
						break;
					case "DisparitionVegetariens.MTS":
						charalDV++;
						break;
					case "CharalLeChien.MTS":
						charalChien++;
						break;
					case "jeuDeLOie.MTS":
						jdO++;
						break;
					case "CaDetend.MTS":
						liptonCD++;
						break;
					case "PandaKitKat.MTS":
						pandaKK++;
						break;
				}
			}
			total++;
			System.out.println(total);
			bufferedReader.close();
			
		}
		
		File file = new File("videogenFiles/empiricalVariante3.csv");
		
		PrintWriter pw= null;
		try {
			 pw = new PrintWriter(file);
		} catch (FileNotFoundException e) {e.printStackTrace();}
		StringBuilder sb = new StringBuilder();
		
		sb.append("Total,Orangina,OranginaRouge,CharalGepar,CharalMordus,DisparitionVegeratiens,CharalLeChien,JeuDeLOie,CaDetend,PandaKitKat, \n");
		sb.append(total+","+orangina+","+oranginaR+","+charalG+","+charalMord+","+charalDV+","+charalChien+","+jdO+","+liptonCD+","+pandaKK+", \n");
		sb.append(100+","+orangina*100/total+","+oranginaR*100/total+","+charalG*100/total+","+charalMord*100/total+","+charalDV*100/total+","+charalChien*100/total+","+jdO*100/total+","+liptonCD*100/total+","+pandaKK*100/total+", \n");
		
		
		// écriture dans le fichier
		pw.write(sb.toString());
        pw.close();
		
	}
	
	
}
