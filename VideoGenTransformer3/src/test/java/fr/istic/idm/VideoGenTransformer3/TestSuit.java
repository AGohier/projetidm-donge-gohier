package fr.istic.idm.VideoGenTransformer3;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import fr.istic.idm.VideoGenTransformer3.model.VideoGenTest1;
import fr.istic.idm.VideoGenTransformer3.model.VideoGenerator;
import fr.istic.idm.VideoGenTransformer3.model.VideoUtilities;

@RunWith(Suite.class)
@Suite.SuiteClasses({
	VideoGenTest1.class,
	VideoGenerator.class,
	VideoUtilities.class
	
})

public class TestSuit {

}
