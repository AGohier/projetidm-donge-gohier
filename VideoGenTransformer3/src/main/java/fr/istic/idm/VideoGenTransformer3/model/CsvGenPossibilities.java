package fr.istic.idm.VideoGenTransformer3.model;



import fr.istic.videoGen.AlternativesMedia;
import fr.istic.videoGen.MandatoryMedia;
import fr.istic.videoGen.OptionalMedia;
import fr.istic.videoGen.VideoGeneratorModel;
import fr.istic.videoGen.MediaDescription;
import fr.istic.videoGen.ImageDescription;
import fr.istic.videoGen.VideoDescription;

import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import java.io.FileNotFoundException;
import java.io.PrintWriter;

public class CsvGenPossibilities {
	private VideoGeneratorModel videogen;
	
	/**
	 * generate an object holding the VideoGeneratorModel for further analysis
	 * @param videogeneratormodel
	 */
	CsvGenPossibilities (VideoGeneratorModel videogeneratormodel){
		this.videogen=videogeneratormodel;
	}
	
	/**
	 * return the number of possible combinations to make a video
	 * @return nbVariantes
	 */
	public int nbVariantes() {
		int toReturn=1;
		
		for(fr.istic.videoGen.Media media: this.videogen.getMedias()) {
			if (media instanceof MandatoryMedia) {
			}
			if (media instanceof OptionalMedia) {
				toReturn = toReturn*2 ;
			}
			if (media instanceof AlternativesMedia) {
				AlternativesMedia alternativesMedia = (AlternativesMedia) media;
				List<MediaDescription> mediaDescriptions = alternativesMedia.getMedias();
				toReturn = toReturn*mediaDescriptions.size();
			}
		}
		
		return toReturn;
	}
	
	/**
	 * generate a csv file displaying details on every possible video to generate at "videogenFiles/videoSet.csv"
	 */
	public void parse () {
		
		List<Media> listMedias = new ArrayList<Media>();
		List<List<Boolean>> listSelect = new ArrayList<List<Boolean>>();
		
		File file = new File("videogenFiles/videoSet.csv");
		
		
		this.videogen.getMedias().forEach(media -> {
			if (media instanceof MandatoryMedia) {
				MandatoryMedia mandatoryMedia = (MandatoryMedia) media;
				MediaDescription mediaDescription = mandatoryMedia.getDescription() ;
				Media mediaLabel=null;
				
				mediaLabel = getMediaFromMediaDescription(mediaDescription);
				
				if(mediaLabel != null) {
					// ajout de l'étiquette à la liste
					listMedias.add(mediaLabel);
					
					// ajout de true aux listes de sélection
					if(listSelect.isEmpty()) {//création d'une première liste si innexistante
						List<Boolean> boolList = new ArrayList<Boolean>();
						boolList.add(new Boolean(true));
						listSelect.add(boolList);
					}else {
						listSelect.forEach(list -> {
							list.add(new Boolean(true));
						});
					}
				}
				
			}
			if (media instanceof OptionalMedia) {
				OptionalMedia optionalMedia = (OptionalMedia) media;
				MediaDescription mediaDescription = optionalMedia.getDescription() ;
				Media mediaLabel=null;
				
				mediaLabel = getMediaFromMediaDescription(mediaDescription);
				
				if(mediaLabel != null) {
					// ajout de l'étiquette à la liste
					listMedias.add(mediaLabel);
					
					// ajout aux listes de sélection
					if(listSelect.isEmpty()) {//création d'une première liste si innexistante
						// créé un cas d'ajout
						List<Boolean> boolList = new ArrayList<Boolean>();
						boolList.add(new Boolean(true));
						listSelect.add(boolList);
						
						// créé un cas de non ajout
						boolList = new ArrayList<Boolean>();
						boolList.add(new Boolean(false));
						listSelect.add(boolList);
					}else {
						List<List<Boolean>> tempList =new ArrayList<List<Boolean>>(); 
						listSelect.forEach(boolList -> {
							List<Boolean> boolListCopy = copyBoolList(boolList);
							// duplication de la liste avec ajout du cas de non-ajout
							boolListCopy.add(new Boolean(false));
							tempList.add(boolListCopy);
							// ajout du cas d'ajout à la liste existante
							boolList.add(new Boolean(true));
						});
						listSelect.addAll(tempList);
					}
				}
				
			}
			if (media instanceof AlternativesMedia) {
				AlternativesMedia alternativesMedia = (AlternativesMedia) media;
				String primId = alternativesMedia.getId();
				List<MediaDescription> mediaDescriptions = alternativesMedia.getMedias();
				
				for(MediaDescription mediaDescription : mediaDescriptions){
					Media mediaLabel=null;
					mediaLabel = getMediaFromMediaDescription(mediaDescription);
						
					if(mediaLabel != null) {
						// édition du nom de l'étiquette
						mediaLabel.setName(primId+" "+mediaLabel.getName());
						// ajout de l'étiquette à la liste
						listMedias.add(mediaLabel);
					}
				}
				int size = mediaDescriptions.size();
				
				List<List<Boolean>> listeTempo = new ArrayList<List<Boolean>>();
				
				for(int i=0;i<size;i++) {//créer n=size liste de boolean
					List<Boolean> listBool = new ArrayList<Boolean>();
					for(int j=0;j<size;j++) {//de taille n'=size d'indice compteur = true
						if(i==j) {
							listBool.add(new Boolean(true));
						}else {
							listBool.add(new Boolean(false));
						}
					}
					listeTempo.add(listBool);
				}
				
				
				
				if(listSelect.isEmpty()){
					for(List<Boolean>list:listeTempo) {
						// ajoute les listes de boolean à liste select is premier est alternative
						listSelect.add(list);
					}
					
				}else {
					List<List<Boolean>> toAddToListSelect = new ArrayList<>();
					for(List<Boolean>listBool : listSelect) {
						
						List<List<Boolean>>	toAddToConstruct = copyListOfListBoolean(listeTempo);
						for(int i = 1 ;i<(listeTempo.size());i++) {
							List<Boolean> temp = copyBoolList(listBool);
							temp.addAll(toAddToConstruct.get(i));
							toAddToListSelect.add( temp );
						}
						listBool.addAll(toAddToConstruct.get(0));
					}
					listSelect.addAll(toAddToListSelect);
				}
			}
		});
		PrintWriter pw= null;
		try {
			 pw = new PrintWriter(file);
		} catch (FileNotFoundException e) {e.printStackTrace();}
		StringBuilder sb = new StringBuilder();
		// génération de la première ligne
		
		
		if(!listMedias.isEmpty()) {
			boolean bool = true;
			Iterator<Media> iterator = listMedias.iterator();
			Media media;
			
			media = iterator.next();
			sb.append("id,");
			
			while(bool) {
				sb.append(media.name);
				sb.append(",");
				
				if(iterator.hasNext()) 
				{media = iterator.next();}
				else {
					sb.append("size\n");
					bool= false;
				}
			}
			
			
		} else { sb.append("id,size\n");}
		
		// génération des lignes suivantes
		int idCount = 0;
		for(List<Boolean> list : listSelect) {
			int lineDuration = 0;
			int lineCount = 0;
			idCount++;
			sb.append(idCount);
			for(Boolean bool:list) {
				
				if(bool) {
					sb.append(",TRUE");
					lineDuration += listMedias.get(lineCount).getDuration();
				}else {
					sb.append(",FALSE");
				}
				lineCount++;
			}
			sb.append(","+lineDuration+"\n");
		}
		
		// écriture dans le fichier
		pw.write(sb.toString());
        pw.close();
		
	}
	
	private List<Boolean> copyBoolList(List<Boolean> listeDeListe){
		List<Boolean> toReturn = new ArrayList<Boolean>();
		listeDeListe.forEach(liste -> {
				toReturn.add(new Boolean(liste.booleanValue()));
		});
		
		return toReturn;
	}
	private List<List<Boolean>> copyListOfListBoolean ( List<List<Boolean>> list){
		 List<List<Boolean>> toReturn = new ArrayList<List<Boolean>>();
		 for(List<Boolean> subL:list) {
			 toReturn.add(copyBoolList(subL));
		 }
		 return toReturn;
	}
	
	private Media getMediaFromMediaDescription(MediaDescription mediaDescription) {
		Media mediaLabel = null;
		if(mediaDescription instanceof ImageDescription) {
			ImageDescription imageDescription = (ImageDescription) mediaDescription;
			mediaLabel = new Media(imageDescription.getImageid(),0);  // Impossible de récupérer la taille d'une image
		}
		
		if(mediaDescription instanceof VideoDescription) {
			VideoDescription videoDescription = (VideoDescription) mediaDescription;
			mediaLabel = new Media(videoDescription.getVideoid(),videoDescription.getDuration());
		}
		return mediaLabel;
	}
	
	private class Media {
		String name;
		int size;
		int duration;
		Media(String name, int duration){
			this.name= name;
			this.duration= duration;
		}
		public String getName(){return this.name;}
		public int getSize() {return this.size;}
		public int getDuration() {return this.duration;}
		public void setName(String name) {this.name=name;}
		public void setSize(int size) {this.size=size;}
		public void setDuration(int duration) {this.duration=duration;}
	}

}
