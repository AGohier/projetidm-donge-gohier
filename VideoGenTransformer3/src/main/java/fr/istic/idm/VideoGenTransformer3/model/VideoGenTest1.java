package fr.istic.idm.VideoGenTransformer3.model;

import static org.junit.Assert.*;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import org.eclipse.emf.common.util.URI;
import org.junit.Test;

import fr.istic.videoGen.VideoGeneratorModel;

public class VideoGenTest1 {
	/**
	 * test the possibility to explore the videogen model
	 */
	@Test
	public void testRunParser() {
		
		VideoGeneratorModel videoGen = new VideoGenHelper().loadVideoGenerator(URI.createURI("videoModel/MaVideo.videogen"));
		assertNotNull(videoGen);
		
		Parser parser = new Parser(videoGen);
		parser.parse();
		
		//System.out.println(videoGen.getInformation().getAuthorName());		
		// and then visit the model
		// eg access video sequences: 
		// videoGen.getVideoseqs()
		
	}
	
	/**
	 * Generate a csv from a model displaying every possible video.
	 * assert it exist
	 * then calculate the number of possibilities
	 * check the consistency of this number with the number of line from the csv
	 * @throws IOException
	 */
	@Test
	public void testGenerateCSV() throws IOException {
		
		VideoGeneratorModel videoGen = new VideoGenHelper().loadVideoGenerator(URI.createURI("videoModel/MaVideo.videogen"));
		assertNotNull(videoGen);
		
		CsvGenPossibilities  parser = new CsvGenPossibilities(videoGen);
		parser.parse();
		File csv = new File("videogenFiles/videoSet.csv");
		assertTrue(csv.exists());
		
		BufferedReader bufferedReader = new BufferedReader(new FileReader(csv));
		String line;
		int count=-1;
		while((line=bufferedReader.readLine()) != null) {
			count++;
		}
		bufferedReader.close();
		int nbCount = parser.nbVariantes();
		System.out.println("Possibilitées calculées: "+nbCount+" LignesCSV : "+ count);
		assertTrue(count == nbCount);
		
		
	}

	
	
}



