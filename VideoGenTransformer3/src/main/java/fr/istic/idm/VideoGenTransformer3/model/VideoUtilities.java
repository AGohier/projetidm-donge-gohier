package fr.istic.idm.VideoGenTransformer3.model;


import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;


import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.URI;
import org.junit.Test;

import fr.istic.videoGen.AlternativesMedia;
import fr.istic.videoGen.MandatoryMedia;
import fr.istic.videoGen.OptionalMedia;
import fr.istic.videoGen.VideoGeneratorModel;
import fr.istic.videoGen.MediaDescription;
import fr.istic.videoGen.ImageDescription;
import fr.istic.videoGen.VideoDescription;
import fr.istic.videoGen.Media;


public class VideoUtilities {

	/**
	 *  Read a video using MPlayer system installation
	 * @param videoPath
	 * @throws IOException
	 * @throws InterruptedException
	 */
	public void lireVideo(String videoPath) throws IOException, InterruptedException {
	
		Runtime run = Runtime.getRuntime();
		Process p = run.exec("mplayer "+ videoPath);
		p.waitFor();	
	}
	
	/**
	 * Take a .txt playLst to read a sequence of video using MPlayer system installation
	 * @param playlist
	 * @throws IOException
	 * @throws InterruptedException
	 */
	public void lirePlaylist(String playlist) throws IOException, InterruptedException {
		
		Runtime run = Runtime.getRuntime();
		Process p = run.exec("mplayer -playlist "+ playlist);
		p.waitFor();
	}
	
	
	private int getRandomInt(int nbPossibilities) {
		
		Random random = new Random();
		int nb = random.nextInt();
		if (nb<0) nb = (nb * -1);
		return nb  % nbPossibilities;
	}
	
	/**
	 * Use ffmpeg to get the length of the considered video
	 * @param videoPath
	 * @return duration of the video
	 * @throws IOException
	 * @throws InterruptedException
	 */
	public float getVideoDuration( String videoPath ) throws IOException, InterruptedException {
        
		Runtime run = Runtime.getRuntime();
		Process p = run.exec("ffprobe -v error -show_entries format=duration -of default=noprint_wrappers=1:nokey=1 poolVideo/" + videoPath);
		BufferedReader output = new BufferedReader(new InputStreamReader(p.getInputStream()));
        String ligne = output.readLine();		
		p.waitFor();
		
		float nb = 0;
		if (ligne != null && ligne != "")
			nb = new Float(ligne).floatValue();
		
		//System.out.println(videoPath + " : " + nb);
		return nb;
	}
	
	/**
	 * Sum up the length of the longer videos from the longer sequence possible
	 * @param videoGen
	 * @return Duration of the longer video possible
	 * @throws IOException
	 * @throws InterruptedException
	 */
	public float dureeVarianteLaPlusLongue(VideoGeneratorModel videoGen ) throws IOException, InterruptedException {
		
		float toReturn = 0;

		for (Media m : videoGen.getMedias()) {
			
			if (m instanceof MandatoryMedia){
				MandatoryMedia mand = (MandatoryMedia)m;
				MediaDescription md = mand.getDescription();
				float temp= getVideoDuration( md.getLocation());
				toReturn+=temp;
				
			}else if (m instanceof OptionalMedia){
				OptionalMedia option =(OptionalMedia)m;
				MediaDescription md = option.getDescription();
				float temp= getVideoDuration( md.getLocation());
				toReturn+=temp;
				
			}else if (m instanceof AlternativesMedia){
				AlternativesMedia alter = (AlternativesMedia)m;
				EList<MediaDescription> liste = alter.getMedias();
				List<Float> listDuration = new ArrayList<Float>();
				
				for( MediaDescription md : liste ) {
					if(md instanceof ImageDescription) {
						// rien
					}else if(md instanceof VideoDescription) {
						VideoDescription video = (VideoDescription)md;
						listDuration.add(new Float(getVideoDuration( video.getLocation())));
					}
				}
				toReturn += listDuration.stream().reduce(Float::max).get().floatValue();
				
			}
		}
		return toReturn;
	}
	
	/**
	 * Generate a single video from videos listed in a .txt list at the specified location and format
	 * @param listeMedia
	 * @param output
	 * @throws IOException
	 * @throws InterruptedException
	 */
	public static void concatenerMedia(List<String> listeMedia , String output) throws IOException, InterruptedException {
		
		Runtime run = Runtime.getRuntime();
		
		BufferedWriter bw = new BufferedWriter(new FileWriter("poolVideo/concatList.txt"));
		for (String path:listeMedia) {
			bw.write("file '" + path  + "\'\n");
		}
		bw.close();
		Process p = run.exec("ffmpeg -y -f concat -i poolVideo/concatList.txt -c copy " + output);
		p.waitFor();
	}
	
	/*L
	 * Generate a random playList from the specified Model
	 * @param videoGen
	 * @return the playList (.txt)
	 * @throws IOException
	 */
	public File generatePlaylist(VideoGeneratorModel videoGen ) throws IOException {
		
		File file = new File("poolVideo/play.txt");
		BufferedWriter bufferedWiter = new BufferedWriter(new FileWriter(file));

		for (Media media : videoGen.getMedias()) {
			
			if (media instanceof MandatoryMedia){
				MandatoryMedia mand = (MandatoryMedia)media;
				MediaDescription md = mand.getDescription();
				bufferedWiter.write( md.getLocation() + "\n");		// Obligatoire
				
			}else if (media instanceof OptionalMedia){
				OptionalMedia option = (OptionalMedia)media;
				MediaDescription md = option.getDescription();
				
				if (getRandomInt(2) == 0)			// 50%
					bufferedWiter.write( md.getLocation() + "\n");
				
			}else if (media instanceof AlternativesMedia){
				AlternativesMedia alter = (AlternativesMedia)media;
				EList<MediaDescription> liste = alter.getMedias();
				
				
				// créé une liste de plage de nombres correspondant à chaque video
				List<int[]> weightList = new ArrayList<int[]>();
				int count = 0;
				
				for(MediaDescription mediaD : liste) {
					if(mediaD instanceof VideoDescription) {
						VideoDescription videoD = (VideoDescription)mediaD;
						int proba = videoD.getProbability();
						if(proba==0) {proba=2;}
						weightList.add(new int[]{count,count+proba});
						count= count+proba+1;
						
					}
				}
				// tirage de la video
				int rand = getRandomInt(count);
				int indexOfVideo=0;
				boolean found=false;
				for(int i=0; i<weightList.size() && !found;i++) {
					if(rand>=weightList.get(i)[0]&& rand<=weightList.get(i)[1]) {
						found=true;
					}else{
						indexOfVideo++;
					}
				}
				// ajout de la video
				bufferedWiter.write(liste.get(indexOfVideo).getLocation() + "\n");
			}
		}
		bufferedWiter.close();
		return file;
	}
	
	/**
	 * Generate a .png file from the specified video at "src/main/webapp/public/thumbnail/video/"
	 * @param videoPath
	 * @return path to the png from "src/main/webapp/public/"
	 * @throws InterruptedException
	 * @throws IOException
	 */
	public static String videoToImage(String videoPath) throws InterruptedException, IOException {
		// System.out.println("CouCou "+videoPath);
		String newImage = "thumbnail/video/" + videoPath.substring(0, videoPath.length()-4) + ".png";
		// System.out.println("CouCou "+"ffmpeg -y -i poolVideo/"+ videoPath + " -r 1 -t 00:00:01 -ss 00:00:02 -f image2 "+newImage);
		Runtime run = Runtime.getRuntime();
		Process p = run.exec("ffmpeg -y -i poolVideo/"+ videoPath + " -r 1 -t 00:00:01 -ss 00:00:02 -f image2 src/main/webapp/public/"+newImage );
		p.waitFor();
		return newImage;
	}
	
	/**
	 * try to run "poolVideo/Orangina.MTS" video using MPlayer
	 * @throws IOException
	 * @throws InterruptedException
	 */
	@Test
	public void testRunVideo() throws IOException, InterruptedException {
		
		lireVideo("poolVideo/Orangina.MTS");
	}
	
	/**
	 * try to run "poolVideo/play.txt" playList using MPlayer 
	 * @throws IOException
	 * @throws InterruptedException
	 */
	@Test
	public void testRunPlaylist() throws IOException, InterruptedException {
		
		lirePlaylist("poolVideo/play.txt");
	}
	
	/**
	 * Generate a playlistFile and assert it exist
	 * @throws IOException
	 */
	@Test
	public void testGeneratePlaylist() throws IOException {
		VideoGeneratorModel videoGen = new VideoGenHelper().loadVideoGenerator(URI.createURI("videoModel/MaVideo.videogen"));
		assertNotNull(videoGen);
		generatePlaylist(videoGen);
		File file = new File("poolVideo/play.txt");
		assertTrue(file.exists());
	}
	
	/**
	 *  checks the length of a known video
	 * @throws IOException
	 * @throws InterruptedException
	 */
	@Test
	public void testDurationVideo() throws IOException, InterruptedException {
		
		float nb = getVideoDuration("Orangina.mp4");
		System.out.println(nb);
		assertEquals(29.982, nb, 0.001);
	}
	
	/**
	 * generate an image from a video, assert it exists then delete it
	 * @throws IOException
	 * @throws InterruptedException
	 */
	@Test
	public void testVideoToImage() throws IOException, InterruptedException {
		
		String img = videoToImage("Orangina.mp4");
		System.out.println( "Image generee : " + img );
		File image = new File("src/main/webapp/public/"+img);
		assertTrue(image.exists());
		image.delete();
	}
	
	/**
	 * compare the calculated max video length to an empirical measurement
	 * @throws IOException
	 * @throws InterruptedException
	 */
	@Test
	public void testRechercheDureelaPlusLongue() throws IOException, InterruptedException {
		
		VideoGeneratorModel videoGen = new VideoGenHelper().loadVideoGenerator(URI.createURI("videoModel/MaVideo.videogen"));
		assertNotNull(videoGen);
		float duree = dureeVarianteLaPlusLongue(videoGen);
		System.out.println("Durée de la variante la plus longue: "+ duree);
		assertEquals(205, duree, 0.5);
	}
	
}
