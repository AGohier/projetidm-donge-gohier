package fr.istic.idm.VideoGenTransformer3.model;


import static org.junit.Assert.assertTrue;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.UUID;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.URI;
import org.junit.Test;

import fr.istic.videoGen.VideoGeneratorModel;
import fr.istic.idm.VideoGenTransformer3.web.ShortMedia;
import fr.istic.videoGen.AlternativesMedia;
import fr.istic.videoGen.MandatoryMedia;
import fr.istic.videoGen.Media;
import fr.istic.videoGen.MediaDescription;
import fr.istic.videoGen.OptionalMedia;

import fr.istic.idm.VideoGenTransformer3.web.ShortMedia;
import fr.istic.videoGen.VideoDescription;

public class VideoGenerator {

	static VideoGeneratorModel modelVideoGen = new VideoGenHelper().loadVideoGenerator(URI.createURI("videoModel/MaVideo.videogen"));
	
	public static VideoGeneratorModel getVideoGeneratorModel() {
		return modelVideoGen;
	}
	

	public static String generateVideo(VideoGeneratorModel videoGen) {
		// créé un nouveau nom aléatoire
		String uuid = UUID.randomUUID().toString();
		String uid = uuid.substring(0, 8)+uuid.substring(24, 32);
		String output = "random"+".MTS";
		try {
			VideoGenerator videoG = new VideoGenerator();
			videoG.generation(videoGen,"src/main/webapp/public/video/" + output);
		}catch (Exception e) {
			System.err.println("Erreur Generation Video " );
			e.printStackTrace();
		}
		
		return output;
	}
	
	
	/*
	 *  Generate video from a .videogen
	 */
	public void generation(VideoGeneratorModel videoGen, String output) throws Exception {
		
		List<String> listLocation = new ArrayList<String>();
		
		
		for (Media media : videoGen.getMedias()) {
			
			if (media instanceof MandatoryMedia){
				MandatoryMedia mand = (MandatoryMedia)media;
				MediaDescription md = mand.getDescription();

				listLocation.add( md.getLocation());
				
			}else if (media instanceof OptionalMedia){
				OptionalMedia option = (OptionalMedia)media;
				MediaDescription md = option.getDescription();
				
				if (getRandomInt(2) == 0)			// 50%

					listLocation.add( md.getLocation());
				
			}else if (media instanceof AlternativesMedia){
				AlternativesMedia alter = (AlternativesMedia)media;
				EList<MediaDescription> liste = alter.getMedias();
				
				
				// créé une liste de plage de nombres correspondant à chaque video
				List<int[]> weightList = new ArrayList<int[]>();
				int count = 0;
				
				for(MediaDescription mediaD : liste) {
					if(mediaD instanceof VideoDescription) {
						VideoDescription videoD = (VideoDescription)mediaD;
						int proba = videoD.getProbability();
						if(proba==0) {proba=2;}
						weightList.add(new int[]{count,count+proba});
						count= count+proba+1;
						
					}
				}
				// tirage de la video
				int rand = getRandomInt(count);
				int indexOfVideo=0;
				boolean found=false;
				for(int i=0; i<weightList.size() && !found;i++) {
					if(rand>=weightList.get(i)[0]&& rand<=weightList.get(i)[1]) {
						found=true;
					}else{
						indexOfVideo++;
					}
				}
				// ajout de la video
				listLocation.add( liste.get(indexOfVideo).getLocation());
			}
		}
		
		
		VideoUtilities.concatenerMedia(listLocation, output);

	}
	
	/**
	 *  Export a videogen file for the web interface
	 */
	public static List<ShortMedia> getListeVideo(VideoGeneratorModel videoGen) throws Exception {
		
		List<ShortMedia> listMedia = new ArrayList<ShortMedia>();
		
		for (Media m : videoGen.getMedias()) {
			listMedia.add(new ShortMedia(m));
		}
		return listMedia;
	}
	

	/**
	 *  Generate a random number like new Random().nextInt(9999)
	 */
	public int getRandomInt(int nbPossibilities) {
		
		Random random = new Random();
		int nb = random.nextInt();
		if (nb<0) nb = (nb * -1);
		return nb  % nbPossibilities;
	}
	
	
	/**
	 * Generate a random video, test it existance and then supress it
	 * @throws Exception
	 */
	@Test
	public void testGeneration() throws Exception {
		VideoGeneratorModel modelVideoGen = new VideoGenHelper().loadVideoGenerator(URI.createURI("videoModel/MaVideo.videogen"));
		generation(modelVideoGen,"src/main/webapp/public/video/test.mp4");
		File file = new File("src/main/webapp/public/video/test.mp4");
		assertTrue(file.exists());
		file.delete();
		
	}

}
