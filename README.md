# Rapport IDM 
### ILA / Donge - Gohier / 2018-2019

## Le générateur de vidéo 
### Stack technique 

 - Ce projet repose sur une grammaire xtext définie ici : fr.istic.videogen/src/fr/istic/VideoGen.xtext
 - Elle est implémenté par les fichiers d'extention .videogen définis ici : VideoGenTransformer3/videoModel
 - Les tests sont implémentés en JUnit4
 - Le site web est réalisé à l'aide de SpringBoot

### Consignes de lancement du projet

 - Cloner le projet et l'ouvrir dans un nouveau workspace Eclipse
 - Lancer le projet VideoGenTransformer3 en tant qu'aplication SpringBoot
 - Le site de l'application est alors disponible sur http://localhost:9200

### Liens vers les éléments du projet

 - Dossier des modèles de video : VideoGenTransformer3/videoModel
 - Dossier des différentes vidéos et des listes de concaténation : VideoGenTransformer3/poolVideo
 - Fichiers CSV produis : VideoGenTransformer3/videogenFiles
 - Vidéos montées pour diffusion : VideoGenTransformer3/src/main/webapp/public/video
 - Images de présentation des vidéos : VideoGenTransformer3/src/main/webapp/public/thumbnail/video

### Fonctions du générateur et leur test

 - Calcul du nombre de variantes possibles
 - Génération d'un CSV récapitulant toutes les variantes possibles
 
 -> Contrôle de la cohérence entre ces deux fonctionnalités
 
 - Génère une video aléatoirement à partir du modèle par défaut
 
 -> Contrôle par un test de génération de test.mp4

 - Lire une vidéo
 - Une suite de vidéo
 
 -> testé par la lescure de ces vidéos avec MPlayer

 - déterminer la longueur d'une vidéo
 
 -> testé par la comparaison à une longeur connue 
 
 - évaluer la longueur de la plus longue vidéo à l'aide de ffmpeg
 
 -> testé par comparaison à une longueur connue
 
 - construir une vidéo à partir d'une liste de vidéo d'origine
 
 -> utilisé dans la génération d'une vidéo aléatoire/ génération d'une vidéo à partir d'une playlist

### Javadoc

VideoGenTransformer3/javadoc


### Tests

L'ensemble des tests unitaires sont regroupés dans la suite de test: VideoGenTransformer3/src/test/java/fr/istic/idm/VideoGenTransformer3/TestSuit.java


## Site Web
Le site est constitué de deux pages:
![page1](captures/page1.png)
Une première qui, à la demande, génère une vidéo selon MaVideo.videogen
![page1-2](captures/page1-2.png)
Le lien permet d'accéder à la lecture de la vidéo produite au format MTS
![page2](captures/page2.png)
La deuxième page permet, quant à elle, de composer sa vidéo dans l'ordre où l'on coche les checkboxes

## Tests et étude empirique
Empirique de la robustesse de notre générateur 

### Test avec de multiples modèles

 - Le modèle standard (MaVideo.videogen) utilise ici un mendatory média en premier. Il comporte deux optionnels suivant un mandatory ou une alternative.
 Il comporte deux alternatives à deux ou quatres dont les poids sont déclarés ou non.
 (Le poids standard utilisé par défaut en absence de paramètres est 2)

 - La variante1 est vide:
 
 Notre implémentation ne supporte pas la soumission d'un modèle vide.
 
 - La variante2 comence par un optionnel. Un mandatory (Orangina) y suit également un optionnel et un alternatif

 |Total	|Orangina  |OranginaRouge |JeuDeLOie	|CaDetend	|
 |------|----------|--------------|-------------|-----------|
 |100	|200	   |40            |54	        |46         |
 |100	|200	   |40	          |54 	        |46         |
 
 Résultats concluants
 
 - La variante3 commence par un alternatif
 
 |Total	|Orangina  |JeuDeLOie	|CaDetend	|
 |------|----------|------------|-----------|
 |100	|100	   |45          |55	        |
 |100	|100	   |45	        |55 	    |

Résultats concluants

### Test d'abondance de la présence des vidéos dans les différantes variantes

Remarques: 
 - Les traitements du choix entre les différantes alternatives est réalisé en fonction d'une pondération et non en 
fonction de la déclaration de pourcentaqges dans un soucis de robustesse.

 

|Total	|Orangina  |OranginaRouge|	CharalGepar|	CharalMordus   |DisparitionVegeratiens	|CharalLeChien	|JeuDeLOie	|CaDetend	|PandaKitKat|
|-------|----------|-------------|-------------|-------------------|------------------------|---------------|-----------|-----------|-----------|
|10000	|10000	   |5022	     |2449	       |2580	           |1503	                |3468           |5093	    |4907	    |5048       |
|100	|100	   |50	         |24	       |25	               |15	                    |34	            |50	        |49	        |50         |

Sur 10 000 générations de séquence (même algorithme de génération de la playlist que la génération complète de la vidéo), selon le fichier MaVideo.videogen, les statitistiques de présence des séquences est en adéquation avec les poids relatifs attribués aux vidéos:
 - Orangina est présente à chaque fois
 - OranginaRouge et pandaKitKat sont optionnels et présentes 50% du temps
 - Gepar, Mordus, DisparitionVégétariens et le Chien qui ont respectivement des poids de 50, 50, 30 et 70 (total: 200) sont présentes à 25, 25, 15 et 35%
 - Les vidéos alternatives du jeu de l'oie et çaDétend sont égalements présentes (50/50)
 
### Difficultés rencontrées

 - La prise en main de ffmpeg n'a pas été évidante. La manipulation de fichiers MP4 ne s'est pas avérée sans problèmes
  (liés aux différantes qualités des vidéos)
 - La manipulation de fichiers d'entrée mp4 à été abandonné pour l'usage de fichiers convertis en MTS qu'il est plus facile de concténer.
 - Les formats MP4 et MPEG ont été testés en premier lieu. Mais le temps nécessaire au traitemnet c'est avéré prohibitif. 