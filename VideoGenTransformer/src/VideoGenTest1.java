import static org.junit.Assert.*;

import org.eclipse.emf.common.util.URI;
import org.junit.Test;

import fr.istic.videoGen.VideoGeneratorModel;

public class VideoGenTest1 {
	
	@Test
	public void testInJava1() {
		
		VideoGeneratorModel videoGen = new VideoGenHelper().loadVideoGenerator(URI.createURI("videoModel/MaVideo.videogen"));
		assertNotNull(videoGen);
		
		Parser parser = new Parser(videoGen);
		parser.parse();
		
		//System.out.println(videoGen.getInformation().getAuthorName());		
		// and then visit the model
		// eg access video sequences: 
		// videoGen.getVideoseqs()
		
	}

	@Test
	public void testInJava2() {
		
		VideoGeneratorModel videoGen = new VideoGenHelper().loadVideoGenerator(URI.createURI("videoModel/MaVideo.videogen"));
		assertNotNull(videoGen);
		
		CsvGenPossibilities  parser = new CsvGenPossibilities(videoGen);
		parser.parse();
		
		//System.out.println(videoGen.getInformation().getAuthorName());		
		// and then visit the model
		// eg access video sequences: 
		// videoGen.getVideoseqs()
		
	}

	@Test
	public void testInJava3() {
		
		VideoGeneratorModel videoGen = new VideoGenHelper().loadVideoGenerator(URI.createURI("videoModel/MaVideo.videogen"));
		assertNotNull(videoGen);
		
		CsvGenPossibilities  parser = new CsvGenPossibilities(videoGen);
		System.out.println(parser.nbVariantes());
		
		//System.out.println(videoGen.getInformation().getAuthorName());		
		// and then visit the model
		// eg access video sequences: 
		// videoGen.getVideoseqs()
		
	}
	
}



