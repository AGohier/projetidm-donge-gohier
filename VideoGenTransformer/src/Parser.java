import org.hamcrest.core.IsInstanceOf;

import fr.istic.videoGen.AlternativesMedia;
import fr.istic.videoGen.MandatoryMedia;
import fr.istic.videoGen.OptionalMedia;
import fr.istic.videoGen.VideoGeneratorModel;
import fr.istic.videoGen.MediaDescription;
import java.util.List;

public class Parser {
	private VideoGeneratorModel videogen;
	
	Parser (VideoGeneratorModel videogeneratormodel){
		this.videogen=videogeneratormodel;
	}
	
	public void parse () {
		this.videogen.getMedias().forEach(media -> {/*
			if (media instanceof MandatoryMedia) {
				System.out.println(((MandatoryMedia) media).getDescription().getLocation());
				
			}
			else if (media instanceof OptionalMedia) {
				System.out.println(((OptionalMedia) media).getDescription().getLocation());
				
			}
			else if (media instanceof AlternativesMedia) {
			}
			*/
			if (media instanceof MandatoryMedia) {
				MandatoryMedia mandatoryMedia = (MandatoryMedia) media;
				System.out.println(mandatoryMedia.getDescription().getLocation());
			}
			if (media instanceof OptionalMedia) {
				OptionalMedia optionalMedia = (OptionalMedia) media;
				System.out.println(optionalMedia.getDescription().getLocation());
			}
			if (media instanceof AlternativesMedia) {
				AlternativesMedia alternativesMedia = (AlternativesMedia) media;
				parseMediaDescriptions(alternativesMedia.getMedias());
			}
		});
		
	}
	
	private void parseMediaDescriptions(List<MediaDescription> mediaDescriptions) {
		mediaDescriptions.forEach(m -> System.out.println(m.getLocation()));
	}

}
