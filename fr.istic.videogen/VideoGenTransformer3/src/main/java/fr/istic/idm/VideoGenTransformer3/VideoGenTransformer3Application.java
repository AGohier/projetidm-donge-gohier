package fr.istic.idm.VideoGenTransformer3;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class VideoGenTransformer3Application {

	public static void main(String[] args) {
		SpringApplication.run(VideoGenTransformer3Application.class, args);
	}

}

